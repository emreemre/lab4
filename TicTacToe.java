import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = {{' ',' ',' '}, {' ',' ',' '}, {' ',' ',' '}};

		printBoard(board);

		boolean isTrue = true;

		while (isTrue) {


			int row;
			int col;

            if (board[0][0] != ' ' && board[0][1] != ' ' && board[0][2] != ' ' && board[1][0] != ' ' && board[1][1] != ' ' && board[1][2] != ' ' && board[2][0] != ' ' && board[2][1] != ' ' && board[2][2] != ' ') {
                System.out.println("Draw!");
                isTrue = false;

            }
            if (isTrue == false){
            break;
            }

			System.out.print("Player 1 enter row number:");
			row = reader.nextInt();
			System.out.print("Player 1 enter column number:");
			col = reader.nextInt();

			if (board[row -1][col -1] == ' '){
                board[row - 1][col - 1] = 'X';
                printBoard(board);

                for(row =1;row<=3;row++) {

                    col = 1;

                    if ((board[row - 1][col - 1] == board[row - 1][col] && board[row - 1][col - 1] != ' ' ) && (board[row - 1][col - 1] == board[row - 1][col + 1] && board[row - 1][col - 1] != ' ')){
                        System.out.println("Player1 won!");
                        isTrue = false;
                    }

                }

                for(col=1;col<=3;col++){

                    row = 1;

                    if((board[row-1][col-1] == board[row][col-1] && board[row-1][col-1] != ' ')&& (board[row-1][col-1] == board[row+1][col-1] && board[row-1][col-1] != ' ')){
                        System.out.println("Player1 won!");
                        isTrue = false;
                    }

                }

                    if ((board[0][2] == board[1][1] && board[0][2] != ' ') && (board[0][2] == board[2][0] && board[0][2] != ' ')) {
                        System.out.println("Player1 won!");
                        isTrue = false;
                    }



                if ((board[0][0] == board[1][1] && board[0][0] != ' ') && (board[0][0] == board[2][2] && board[0][0] != ' ')) {
                    System.out.println("Player1 won!");
                    isTrue = false;
                }
                if (isTrue == false){
                    break;}

            }else{
                System.out.println("It's not empty!");
            }


            if (board[0][0] != ' ' && board[0][1] != ' ' && board[0][2] != ' ' && board[1][0] != ' ' && board[1][1] != ' ' && board[1][2] != ' ' && board[2][0] != ' ' && board[2][1] != ' ' && board[2][2] != ' ') {
                System.out.println("Draw!");
                isTrue = false;

            }
            if (isTrue == false){
                break;
            }


            System.out.print("Player 2 enter row number:");
			row = reader.nextInt();
			System.out.print("Player 2 enter column number:");
			col = reader.nextInt();




            if (board[row -1][col -1] == ' ') {
                board[row - 1][col - 1] = 'O';
                printBoard(board);

                for(row =1;row<=3;row++) {

                    col = 1;

                    if ((board[row - 1][col - 1] == board[row - 1][col] && board[row - 1][col - 1] != ' ' ) && (board[row - 1][col - 1] == board[row - 1][col + 1] && board[row - 1][col - 1] != ' ')){
                        System.out.println("Player2 won!");
                        isTrue = false;
                    }
                }

                for(col=1;col<=3;col++) {

                    row = 1;

                    if ((board[row - 1][col - 1] == board[row][col - 1] && board[row - 1][col - 1] != ' ') && (board[row - 1][col - 1] == board[row + 1][col - 1] && board[row - 1][col - 1] != ' ')) {
                        System.out.print("Player2 won!");
                        isTrue = false;
                    }
                }
                if ((board[0][2] == board[1][1] && board[0][2] != ' ') && (board[0][2] == board[2][0] && board[0][2] != ' ')) {
                    System.out.print("Player2 won!");
                    isTrue = false;
                }



                if ((board[0][0] == board[1][1] && board[0][0] != ' ') && (board[0][0] == board[2][2] && board[0][0] != ' ')) {
                    System.out.print("Player2 won!");
                    isTrue = false;
                }
                if (isTrue == false){
                    break;
                }


            }else {
                System.out.println("It's not empty!");

            }
		}
		reader.close();

	}


	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
                    System.out.print("|");


			}
			System.out.println();
			System.out.println("   -----------");

		}



	}

}